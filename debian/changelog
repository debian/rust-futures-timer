rust-futures-timer (3.0.3-4) unstable; urgency=medium

  * update DEP-3 patch headers
  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + simplify usage comments
  * update watch file:
    + improve filename mangling
    + use Github API; drop unused repack suffix
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 07 Feb 2025 07:48:44 +0100

rust-futures-timer (3.0.3-3) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * stop mention dh-cargo in long description
  * add patches 1001_*
    to accept newer branches of crates gloo-timers send_wrapper;
    drop obsolete patch 2001;
    (build-)depend on packages for crates gloo-timers send_wrapper
  * provide and autopkgtest feature wasm-bindgen
  * check single-feature autopkgtest tests only on amd64

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 12 Oct 2024 12:54:20 +0200

rust-futures-timer (3.0.3-2) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)
  * declare compliance with Debian Policy 4.7.0
  * relax to autopkgtest-depend unversioned
    when version is satisfied in Debian stable
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 13:27:44 +0200

rust-futures-timer (3.0.3-1) unstable; urgency=medium

  * bump project versions in virtual packages and autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 22 Feb 2024 17:43:28 +0100

rust-futures-timer (3.0.2-3) unstable; urgency=medium

  * update dh-cargo fork;
    closes: bug#1047679, thanks to Lucas Nussbaum
  * update DEP-3 patch headers

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Aug 2023 13:43:16 +0200

rust-futures-timer (3.0.2-2) unstable; urgency=medium

  * tighten autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 18:17:52 +0100

rust-futures-timer (3.0.2-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * bump version for provided virtual packages and autopkgtest hints
  * change binary library package to be arch-independent
  * declare compliance with Debian Policy 4.6.2
  * stop superfluously provide virtual unversioned feature package
  * update dh-cargo fork
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Feb 2023 17:16:38 +0100

rust-futures-timer (3.0.0-4) unstable; urgency=medium

  * fix version for provided virtual packages and autopkgtest hints
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 04 Dec 2022 12:51:30 +0100

rust-futures-timer (3.0.0-3) unstable; urgency=medium

  * fix avoid autopkgtest for patched-away feature wasm-bindgen

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Dec 2022 10:39:27 +0100

rust-futures-timer (3.0.0-2) unstable; urgency=medium

  * re-release for building with auto-builder

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 10:37:12 +0200

rust-futures-timer (3.0.0-1) unstable; urgency=low

  * initial Release;
    closes: Bug#1022831

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 26 Oct 2022 20:28:05 +0200
